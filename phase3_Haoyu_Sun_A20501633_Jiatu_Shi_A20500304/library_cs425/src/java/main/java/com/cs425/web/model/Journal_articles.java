package com.cs425.web.model;

public class Journal_articles {

    private String journals_article_id;
    private String doc_id;
    private String authors;


    public String getJournals_article_id() {
        return journals_article_id;
    }

    public void setJournals_article_id(String journals_article_id) {
        this.journals_article_id = journals_article_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }
    @Override
    public String toString() {
        return "Journal_articles [journals_article_id=" + journals_article_id + ", doc_id=" + doc_id + "," +
                " authors=" + authors + "]";
    }
}
