package com.cs425.web.model;

public class Document {
    private String doc_id;
    private String title;
    private String keywords;
    private String classification;
    private int citation;
    private String hie_cla_id;

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public int getCitation() {
        return citation;
    }

    public void setCitation(int citation) {
        this.citation = citation;
    }

    public String getHie_cla_id() {
        return hie_cla_id;
    }

    public void setHie_cla_id(String hie_cla_id) {
        this.hie_cla_id = hie_cla_id;
    }



    @Override
    public String toString() {
        return "Document [doc_id=" + doc_id + ", title=" + title + ", keywords=" + keywords + ", classification=" + classification +
                ", citation=" + citation + ", hie_cla_id=" + hie_cla_id + "]";
    }


}
