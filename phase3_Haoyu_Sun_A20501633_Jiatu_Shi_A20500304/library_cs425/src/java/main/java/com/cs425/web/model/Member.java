package com.cs425.web.model;

public class Member {

    private String mem_ID;
    private String name;
    private String phone_number;
    private String email_address;
    private String password;


    public String getMem_ID() {
        return mem_ID;
    }
    public void setMem_ID(String mem_ID) {
        this.mem_ID = mem_ID;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPhone_number() {
        return phone_number;
    }
    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
    public double getEmail_address() {
        return email_address;
    }
    public void setEmail_address(double email_address) {
        this.email_address = email_address;
    }
    public double getPassword() {
        return password;
    }
    public void setPassword(double password) {
        this.password = password;
    }
    @Override
    public String toString() {
        return "Member [mem_ID=" + mem_ID + ", name=" + name + ", phone_number=" + phone_number + ", email_address=" + email_address
                + ", password=" + password + "]";
    }


}
