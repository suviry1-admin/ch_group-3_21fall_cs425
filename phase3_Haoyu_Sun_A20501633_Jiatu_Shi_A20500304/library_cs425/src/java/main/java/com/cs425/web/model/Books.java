package com.cs425.web.model;

public class Books {

    private String book_id;
    private String doc_id,


    private String authors;
    private String publisher;
    private int edition;
    private int publish_year;


    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public int getPublish_year() {
        return publish_year;
    }

    public void setPublish_year(int publish_year) {
        this.publish_year = publish_year;
    }
    @Override
    public String toString() {
        return "Books [book_id=" + book_id + ", doc_id=" + doc_id + ", authors=" + authors + ", publisher=" + publisher +
                ", edition=" + edition + ", publish_year=" + publish_year + "]";
    }


}
