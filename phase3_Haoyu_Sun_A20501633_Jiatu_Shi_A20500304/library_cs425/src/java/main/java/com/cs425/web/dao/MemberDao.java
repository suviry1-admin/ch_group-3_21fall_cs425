package com.cs425.web.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cs425.web.model.Instructor;

public class MemberDao {

    final String url = "jdbc:postgresql://localhost:5432/university";
    final String user = "postgres";
    final String password = "1234";//"<add your password>";

    public Member getMember(String iID) {



        String mySQL = "SELECT mem_id, name, phone_number, email_address, password "
                + "FROM Member "
                + "WHERE mem_id = ?";

        Instructor ob1 = new Member();

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement pStmt = conn.prepareStatement(mySQL)){

            pStmt.setString(1, iID.trim());

            ResultSet rs = pStmt.executeQuery();

            while (rs.next()) {
            	/* Retrieves the value of the designated column in the current row
            	   of this ResultSet object as a String in the Java programming language.
            	*/
                ob1.setID(rs.getString("mem_id"));
                ob1.setName(rs.getString("name"));
                ob1.setDept_name(rs.getString("phone_number"));
                ob1.setSalary(rs.getDouble("email_address"));
                ob1.setPassword(rs.getString("password"))
            }
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return ob1;

    }
}
