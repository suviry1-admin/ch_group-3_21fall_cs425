package com.cs425.web.model;

public class Magazines {

    private String magazines_id;
    private String doc_id,
    private String editors;
    private String contributors;
    private String issue;
    private String publish_time;

    public String getMagazines_id() {
        return magazines_id;
    }

    public void setMagazines_id(String magazines_id) {
        this.magazines_id = magazines_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getEditors() {
        return editors;
    }

    public void setEditors(String editors) {
        this.editors = editors;
    }

    public String getContributors() {
        return contributors;
    }

    public void setContributors(String contributors) {
        this.contributors = contributors;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getPublish_time() {
        return publish_time;
    }

    public void setPublish_time(String publish_time) {
        this.publish_time = publish_time;
    }

    @Override
    public String toString() {
        return "Magazines [magazines_id=" + magazines_id + ", doc_id=" + doc_id + ", editors=" + editors + ", contributors=" + contributors +
                ", issue=" + issue + ", publish_time=" + publish_time + "]";
    }


}
