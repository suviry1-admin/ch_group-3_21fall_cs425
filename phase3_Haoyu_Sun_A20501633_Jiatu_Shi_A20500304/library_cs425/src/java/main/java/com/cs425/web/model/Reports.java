package com.cs425.web.model;

public class Reports {
    private String reports_id;
    private String doc_id,
    private String reporter;
    private String editors;
    private String summary;
    private String report_time;



    public String getReports_id() {
        return reports_id;
    }

    public void setReports_id(String reports_id) {
        this.reports_id = reports_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getEditors() {
        return editors;
    }

    public void setEditors(String editors) {
        this.editors = editors;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getReport_time() {
        return report_time;
    }

    public void setReport_time(String report_time) {
        this.report_time = report_time;
    }
    @Override
    public String toString() {
        return "Reports [reports_id=" + reports_id + ", doc_id=" + doc_id + ", reporter=" + reporter + ", editors=" + editors +
                ", summary=" + summary + ", report_time=" + report_time + "]";
    }


}
