package com.cs425.web.model;

public class Thesis {
    private String thesis_id;
    private String doc_id,
    private String authors;


    private String abstracts;
    private String orgnization;
    public String getAbstracts() {
        return abstracts;
    }

    public void setAbstracts(String abstracts) {
        this.abstracts = abstracts;
    }

    public String getThesis_id() {
        return thesis_id;
    }

    public void setThesis_id(String thesis_id) {
        this.thesis_id = thesis_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getOrgnization() {
        return orgnization;
    }

    public void setOrgnization(String orgnization) {
        this.orgnization = orgnization;
    }

    @Override
    public String toString() {
        return "Thesis [thesis_id=" + thesis_id + ", doc_id=" + doc_id + ", authors=" + authors + ", abstracts=" + abstracts +
                ", orgnization=" + orgnization + "]";
    }


}
