package com.cs425.web.model;

public class Copies {

    private String cop_id;
    private int copies_status;
    private String doc_id;
    private String borrowing_info_id;
    private String bookshelf;
    private int room;
    private int level;

    public String getCop_id() {
        return cop_id;
    }

    public void setCop_id(String cop_id) {
        this.cop_id = cop_id;
    }

    public int getCopies_status() {
        return copies_status;
    }

    public void setCopies_status(int copies_status) {
        this.copies_status = copies_status;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getBorrowing_info_id() {
        return borrowing_info_id;
    }

    public void setBorrowing_info_id(String borrowing_info_id) {
        this.borrowing_info_id = borrowing_info_id;
    }

    public String getBookshelf() {
        return bookshelf;
    }

    public void setBookshelf(String bookshelf) {
        this.bookshelf = bookshelf;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }



    @Override
    public String toString() {
        return "Instructor [ID=" + ID + ", name=" + name + ", dept_name=" + dept_name + ", salary=" + salary + "]";
    }


}
