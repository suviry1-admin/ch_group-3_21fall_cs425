package com.cs425.web.model;

public class Journals {

    private String journal_id;
    private String journals_title;
    private String issue;


    private String journals_article_id;
    private String journals_type;
    private String publisher;

    private String editors;
    private String publish_date;



    public String getJournal_id() {
        return journal_id;
    }

    public void setJournal_id(String journal_id) {
        this.journal_id = journal_id;
    }

    public String getJournals_title() {
        return journals_title;
    }

    public void setJournals_title(String journals_title) {
        this.journals_title = journals_title;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getJournals_article_id() {
        return journals_article_id;
    }

    public void setJournals_article_id(String journals_article_id) {
        this.journals_article_id = journals_article_id;
    }

    public String getJournals_type() {
        return journals_type;
    }

    public void setJournals_type(String journals_type) {
        this.journals_type = journals_type;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getEditors() {
        return editors;
    }

    public void setEditors(String editors) {
        this.editors = editors;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }
    @Override
    public String toString() {
        return "Journals [journal_id=" + journal_id + ", journals_title=" + journals_title + ", issus=" + issue +
                ", journals_article_id=" + journals_article_id + ", journals_type=" + journals_type +
                ", editors=" + editors + "]";
    }
}
